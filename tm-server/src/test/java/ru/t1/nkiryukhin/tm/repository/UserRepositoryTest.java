package ru.t1.nkiryukhin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.nkiryukhin.tm.api.repository.IUserRepository;
import ru.t1.nkiryukhin.tm.api.service.IConnectionService;
import ru.t1.nkiryukhin.tm.api.service.IPropertyService;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.field.AbstractFieldException;
import ru.t1.nkiryukhin.tm.exception.field.UserIdEmptyException;
import ru.t1.nkiryukhin.tm.exception.user.AccessDeniedException;
import ru.t1.nkiryukhin.tm.marker.UnitCategory;
import ru.t1.nkiryukhin.tm.model.User;
import ru.t1.nkiryukhin.tm.service.ConnectionService;
import ru.t1.nkiryukhin.tm.service.PropertyService;

import java.sql.Connection;
import java.sql.SQLException;

import static ru.t1.nkiryukhin.tm.data.UserTestData.*;

@Category(UnitCategory.class)
public final class UserRepositoryTest {

    private static Connection connection;

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @Nullable
    private static IUserRepository repository;

    @BeforeClass
    public static void beforeClass() throws SQLException {
        connection = connectionService.getConnection();
        repository = new UserRepository(connection);
        repository.add(USUAL_USER);
        connection.commit();
    }

    @AfterClass
    public static void afterClass() throws AccessDeniedException, SQLException, UserIdEmptyException {
        repository.removeAll(USER_LIST);
        connection.commit();
        connectionService.closeDatabaseConnectionPool();
    }

    @Test
    public void add() throws AbstractFieldException, SQLException {
        Assert.assertNotNull(repository.add(ADMIN_USER));
        @Nullable final User user = repository.findOneById(ADMIN_USER.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(ADMIN_USER, user);
        connection.rollback();
    }

    @Test
    public void addMany() throws AbstractFieldException, SQLException {
        Assert.assertNotNull(repository.add(ADMIN_USER_LIST));
        for (final User user : ADMIN_USER_LIST)
            Assert.assertEquals(user, repository.findOneById(user.getId()));
        connection.rollback();
    }

    @Test
    public void existsById() {
        Assert.assertFalse(repository.existsById(NON_EXISTING_USER_ID));
        Assert.assertTrue(repository.existsById(USUAL_USER.getId()));
    }

    @Test
    public void findOneById() throws AbstractFieldException {
        Assert.assertNull(repository.findOneById(null));
        Assert.assertNull(repository.findOneById(NON_EXISTING_USER_ID));
        @Nullable final User user = repository.findOneById(USUAL_USER.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(USUAL_USER, user);
    }

    @Test
    public void findOneByIndex() throws AbstractFieldException {
        final int index = repository.findAll().indexOf(USUAL_USER);
        @Nullable final User user = repository.findOneByIndex(index);
        Assert.assertNotNull(user);
        Assert.assertEquals(USUAL_USER, user);
    }

    @Test
    public void remove() throws AbstractException {
        Assert.assertNull(repository.removeOne(null));
        @Nullable final User createdUser = repository.add(ADMIN_USER);
        @Nullable final User removedUser = repository.removeOne(createdUser);
        Assert.assertNotNull(removedUser);
        Assert.assertEquals(ADMIN_USER, removedUser);
        Assert.assertNull(repository.findOneById(ADMIN_USER.getId()));
    }

    @Test
    public void removeById() throws AbstractFieldException {
        Assert.assertNull(repository.removeById(null));
        Assert.assertNull(repository.removeById(NON_EXISTING_USER_ID));
        repository.add(ADMIN_USER);
        @Nullable final User removedUser = repository.removeById(ADMIN_USER.getId());
        Assert.assertNotNull(removedUser);
        Assert.assertEquals(ADMIN_USER, removedUser);
        Assert.assertNull(repository.findOneById(ADMIN_USER.getId()));
    }

    @Test
    public void removeByIndex() throws AbstractFieldException {
        Assert.assertNull(repository.removeByIndex(null));
        @Nullable final User createdUser = repository.add(ADMIN_USER);
        final int index = repository.findAll().indexOf(createdUser);
        @Nullable final User removedUser = repository.removeByIndex(index);
        Assert.assertNotNull(removedUser);
        Assert.assertEquals(ADMIN_USER, removedUser);
        Assert.assertNull(repository.findOneById(ADMIN_USER.getId()));
    }

    @Test
    public void findByLogin() {
        Assert.assertNull(repository.findByLogin(null));
        @Nullable final User user = repository.findByLogin(USUAL_USER.getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals(USUAL_USER, user);
    }

    @Test
    public void findByEmail() {
        Assert.assertNull(repository.findByEmail(null));
        @Nullable final User user = repository.findByEmail(USUAL_USER.getEmail());
        Assert.assertNotNull(user);
        Assert.assertEquals(USUAL_USER, user);
    }

    @Test
    public void removeAll() throws AbstractException, SQLException {
        int userCount = repository.getSize();
        repository.removeAll(USER_LIST);
        Assert.assertEquals(1, userCount - repository.getSize());
        connection.rollback();
    }

    @Test
    public void isLoginExists() {
        Assert.assertFalse(repository.isLoginExist(null));
        Assert.assertTrue(repository.isLoginExist(USUAL_USER.getLogin()));
    }

    @Test
    public void isEmailExists() {
        Assert.assertFalse(repository.isEmailExist(null));
        Assert.assertTrue(repository.isEmailExist(USUAL_USER.getEmail()));
    }

}