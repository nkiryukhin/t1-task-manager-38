package ru.t1.nkiryukhin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.nkiryukhin.tm.api.repository.ITaskRepository;
import ru.t1.nkiryukhin.tm.api.service.IConnectionService;
import ru.t1.nkiryukhin.tm.api.service.IPropertyService;
import ru.t1.nkiryukhin.tm.comparator.NameComparator;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.field.AbstractFieldException;
import ru.t1.nkiryukhin.tm.exception.field.UserIdEmptyException;
import ru.t1.nkiryukhin.tm.exception.user.AccessDeniedException;
import ru.t1.nkiryukhin.tm.marker.UnitCategory;
import ru.t1.nkiryukhin.tm.model.Task;
import ru.t1.nkiryukhin.tm.service.ConnectionService;
import ru.t1.nkiryukhin.tm.service.PropertyService;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Comparator;
import java.util.stream.Collectors;

import static ru.t1.nkiryukhin.tm.data.ProjectTestData.USUAL_PROJECT1;
import static ru.t1.nkiryukhin.tm.data.TaskTestData.*;
import static ru.t1.nkiryukhin.tm.data.UserTestData.ADMIN_USER;
import static ru.t1.nkiryukhin.tm.data.UserTestData.USUAL_USER;

@Category(UnitCategory.class)
public class TaskRepositoryTest {

    private static Connection connection;

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static ITaskRepository repository;

    @BeforeClass
    public static void beforeClass() throws SQLException {
        connection = connectionService.getConnection();
        repository = new TaskRepository(connection);
        repository.add(USUAL_TASK1);
        repository.add(USUAL_TASK2);
        connection.commit();
    }

    @AfterClass
    public static void afterClass() throws AccessDeniedException, SQLException, UserIdEmptyException {
        repository.removeAll(TASK_LIST);
        connection.commit();
        connectionService.closeDatabaseConnectionPool();
    }

    @Test
    public void add() throws AbstractFieldException, SQLException {
        Assert.assertNotNull(repository.add(ADMIN_TASK1));
        @Nullable final Task task = repository.findOneById(ADMIN_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(ADMIN_TASK1, task);
        connection.rollback();
    }

    @Test
    public void addMany() throws AbstractFieldException, SQLException {
        Assert.assertNotNull(repository.add(ADMIN_TASK_LIST));
        for (final Task task : ADMIN_TASK_LIST)
            Assert.assertEquals(task, repository.findOneById(task.getId()));
        connection.rollback();
    }

    @Test
    public void addByUserId() throws AbstractFieldException, SQLException {
        Assert.assertNotNull(repository.add(ADMIN_USER.getId(), ADMIN_TASK1));
        @Nullable final Task task = repository.findOneById(ADMIN_USER.getId(), ADMIN_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(ADMIN_TASK1, task);
        connection.rollback();
    }

    @Test
    public void createByUserId() throws AbstractFieldException {
        @NotNull final Task task = repository.create(ADMIN_USER.getId(), ADMIN_TASK1.getName());
        Assert.assertEquals(task, repository.findOneById(ADMIN_USER.getId(), task.getId()));
        Assert.assertEquals(ADMIN_TASK1.getName(), task.getName());
        Assert.assertEquals(ADMIN_USER.getId(), task.getUserId());
    }

    @Test
    public void createByUserIdWithDescription() throws AbstractFieldException {
        @NotNull final Task task = repository.create(ADMIN_USER.getId(), ADMIN_TASK1.getName(), ADMIN_TASK1.getDescription());
        Assert.assertEquals(task, repository.findOneById(ADMIN_USER.getId(), task.getId()));
        Assert.assertEquals(ADMIN_TASK1.getName(), task.getName());
        Assert.assertEquals(ADMIN_TASK1.getDescription(), task.getDescription());
        Assert.assertEquals(ADMIN_USER.getId(), task.getUserId());
    }

    @Test
    public void findAllByUserId() throws UserIdEmptyException {
        Assert.assertEquals(Collections.emptyList(), repository.findAll(""));
        Assert.assertEquals(USUAL_TASK_LIST, repository.findAll(USUAL_USER.getId()));
    }

    @Test
    public void findAllComparatorByUserId() throws UserIdEmptyException {
        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        Assert.assertEquals(
                USUAL_TASK_LIST.stream().sorted(comparator).collect(Collectors.toList()),
                repository.findAll(USUAL_USER.getId(), comparator)
        );
    }

    @Test
    public void existsById() {
        Assert.assertFalse(repository.existsById(NON_EXISTING_TASK_ID));
        Assert.assertTrue(repository.existsById(USUAL_TASK1.getId()));
    }

    @Test
    public void existsByIdByUserId() throws AbstractFieldException {
        Assert.assertFalse(repository.existsById(USUAL_USER.getId(), NON_EXISTING_TASK_ID));
        Assert.assertTrue(repository.existsById(USUAL_USER.getId(), USUAL_TASK1.getId()));
    }

    @Test
    public void findOneById() throws AbstractFieldException {
        Assert.assertNull(repository.findOneById(NON_EXISTING_TASK_ID));
        @Nullable final Task task = repository.findOneById(USUAL_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USUAL_TASK1, task);
    }

    @Test
    public void findOneByIdByUserId() throws AbstractFieldException {
        Assert.assertNull(repository.findOneById(USUAL_USER.getId(), NON_EXISTING_TASK_ID));
        @Nullable final Task task = repository.findOneById(USUAL_USER.getId(), USUAL_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USUAL_TASK1, task);
    }

    @Test
    public void findOneByIndex() throws AbstractFieldException {
        final int index = repository.findAll().indexOf(USUAL_TASK1);
        @Nullable final Task task = repository.findOneByIndex(index);
        Assert.assertNotNull(task);
        Assert.assertEquals(USUAL_TASK1, task);
    }

    @Test
    public void findOneByIndexByUserId() throws AbstractFieldException {
        final int index = repository.findAll(USUAL_TASK1.getUserId()).indexOf(USUAL_TASK1);
        @Nullable final Task task = repository.findOneByIndex(USUAL_USER.getId(), index);
        Assert.assertNotNull(task);
        Assert.assertEquals(USUAL_TASK1, task);
    }

    @Test
    public void findAllByProjectId() {
        Assert.assertEquals(USUAL_TASK_LIST, repository.findAllByProjectId(USUAL_USER.getId(), USUAL_PROJECT1.getId()));
    }

    @Test
    public void clearByUserId() throws UserIdEmptyException, SQLException {
        repository.clear(USUAL_USER.getId());
        Assert.assertEquals(0, repository.getSize(USUAL_USER.getId()));
        connection.rollback();
    }

    @Test
    public void remove() throws AbstractException {
        @Nullable final Task createdTask = repository.add(ADMIN_TASK1);
        @Nullable final Task removedTask = repository.removeOne(createdTask);
        Assert.assertNotNull(removedTask);
        Assert.assertEquals(ADMIN_TASK1, removedTask);
        Assert.assertNull(repository.findOneById(ADMIN_TASK1.getId()));
    }

    @Test
    public void removeByUserId() throws AbstractFieldException {
        Assert.assertNull(repository.remove(ADMIN_USER.getId(), null));
        @Nullable final Task createdTask = repository.add(ADMIN_TASK1);
        Assert.assertNull(repository.remove(null, createdTask));
        @Nullable final Task removedTask = repository.remove(ADMIN_USER.getId(), createdTask);
        Assert.assertEquals(ADMIN_TASK1, removedTask);
        Assert.assertNull(repository.findOneById(ADMIN_USER.getId(), ADMIN_TASK1.getId()));
    }

    @Test
    public void removeById() throws AbstractFieldException {
        Assert.assertNull(repository.removeById(null));
        Assert.assertNull(repository.removeById(NON_EXISTING_TASK_ID));
        repository.add(ADMIN_TASK1);
        @Nullable final Task removedTask = repository.removeById(ADMIN_TASK1.getId());
        Assert.assertNotNull(removedTask);
        Assert.assertEquals(ADMIN_TASK1, removedTask);
        Assert.assertNull(repository.findOneById(ADMIN_TASK1.getId()));
    }

    @Test
    public void removeByIdByUserId() throws AbstractFieldException {
        Assert.assertNull(repository.removeById(ADMIN_USER.getId(), null));
        Assert.assertNull(repository.removeById(ADMIN_USER.getId(), NON_EXISTING_TASK_ID));
        Assert.assertNull(repository.removeById(ADMIN_USER.getId(), USUAL_TASK1.getId()));
        @Nullable final Task createdTask = repository.add(ADMIN_TASK1);
        Assert.assertNull(repository.removeById(null, createdTask.getId()));
        @Nullable final Task removedTask = repository.removeById(ADMIN_USER.getId(), createdTask.getId());
        Assert.assertNotNull(removedTask);
        Assert.assertEquals(ADMIN_TASK1, removedTask);
        Assert.assertNull(repository.findOneById(ADMIN_USER.getId(), ADMIN_TASK1.getId()));
    }

    @Test
    public void removeByIndex() throws AbstractFieldException {
        Assert.assertNull(repository.removeByIndex(null));
        @Nullable final Task createdTask = repository.add(ADMIN_TASK1);
        final int index = repository.findAll().indexOf(createdTask);
        @Nullable final Task removedTask = repository.removeByIndex(index);
        Assert.assertNotNull(removedTask);
        Assert.assertEquals(ADMIN_TASK1, removedTask);
        Assert.assertNull(repository.findOneById(ADMIN_TASK1.getId()));
    }

    @Test
    public void removeByIndexByUserId() throws AbstractFieldException {
        Assert.assertNull(repository.removeByIndex(ADMIN_USER.getId(), null));
        @Nullable final Task createdTask = repository.add(ADMIN_TASK1);
        final int index = repository.findAll(ADMIN_USER.getId()).indexOf(createdTask);
        Assert.assertNull(repository.removeByIndex(null, index));
        @Nullable final Task removedTask = repository.removeByIndex(ADMIN_USER.getId(), index);
        Assert.assertNotNull(removedTask);
        Assert.assertEquals(ADMIN_TASK1, removedTask);
        Assert.assertNull(repository.findOneById(ADMIN_USER.getId(), ADMIN_TASK1.getId()));
    }

    @Test
    public void getSizeByUserId() throws UserIdEmptyException, SQLException {
        Assert.assertEquals(0, repository.getSize(ADMIN_USER.getId()));
        repository.add(ADMIN_TASK1);
        Assert.assertEquals(1, repository.getSize(ADMIN_USER.getId()));
        connection.rollback();
    }

    @Test
    public void removeAll() throws AccessDeniedException, UserIdEmptyException {
        repository.add(ADMIN_TASK_LIST);
        repository.removeAll(ADMIN_TASK_LIST);
        Assert.assertEquals(0, repository.getSize(ADMIN_USER.getId()));
    }

}
