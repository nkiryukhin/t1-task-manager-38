package ru.t1.nkiryukhin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.nkiryukhin.tm.api.repository.ISessionRepository;
import ru.t1.nkiryukhin.tm.api.service.IConnectionService;
import ru.t1.nkiryukhin.tm.api.service.IPropertyService;
import ru.t1.nkiryukhin.tm.exception.field.AbstractFieldException;
import ru.t1.nkiryukhin.tm.exception.field.UserIdEmptyException;
import ru.t1.nkiryukhin.tm.exception.user.AccessDeniedException;
import ru.t1.nkiryukhin.tm.marker.UnitCategory;
import ru.t1.nkiryukhin.tm.model.Session;
import ru.t1.nkiryukhin.tm.service.ConnectionService;
import ru.t1.nkiryukhin.tm.service.PropertyService;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collections;

import static ru.t1.nkiryukhin.tm.data.SessionTestData.*;
import static ru.t1.nkiryukhin.tm.data.UserTestData.ADMIN_USER;
import static ru.t1.nkiryukhin.tm.data.UserTestData.USUAL_USER;

@Category(UnitCategory.class)
public final class SessionRepositoryTest {

    private static Connection connection;

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static ISessionRepository repository;

    @BeforeClass
    public static void beforeClass() throws SQLException {
        connection = connectionService.getConnection();
        repository = new SessionRepository(connection);
        repository.add(USUAL_SESSION1);
        repository.add(USUAL_SESSION2);
        connection.commit();
    }

    @AfterClass
    public static void afterClass() throws AccessDeniedException, SQLException, UserIdEmptyException {
        repository.removeAll(SESSION_LIST);
        connection.commit();
        connectionService.closeDatabaseConnectionPool();
    }

    @Test
    public void add() throws AbstractFieldException, SQLException {
        Assert.assertNotNull(repository.add(ADMIN_SESSION1));
        @Nullable final Session session = repository.findOneById(ADMIN_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(ADMIN_SESSION1, session);
        connection.rollback();
    }

    @Test
    public void addMany() throws AbstractFieldException, SQLException {
        Assert.assertNotNull(repository.add(ADMIN_SESSION_LIST));
        for (final Session session : ADMIN_SESSION_LIST)
            Assert.assertEquals(session, repository.findOneById(session.getId()));
        connection.rollback();
    }

    @Test
    public void addByUserId() throws AbstractFieldException, SQLException {
        Assert.assertNotNull(repository.add(ADMIN_USER.getId(), ADMIN_SESSION1));
        @Nullable final Session session = repository.findOneById(ADMIN_USER.getId(), ADMIN_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(ADMIN_SESSION1, session);
        connection.rollback();
    }

    @Test
    public void findAllByUserId() throws UserIdEmptyException {
        Assert.assertEquals(Collections.emptyList(), repository.findAll(""));
        Assert.assertEquals(USER_SESSION_LIST, repository.findAll(USUAL_USER.getId()));
    }

    @Test
    public void existsById() {
        Assert.assertFalse(repository.existsById(NON_EXISTING_SESSION_ID));
        Assert.assertTrue(repository.existsById(USUAL_SESSION1.getId()));
    }

    @Test
    public void existsByIdByUserId() throws AbstractFieldException {
        Assert.assertFalse(repository.existsById(USUAL_USER.getId(), NON_EXISTING_SESSION_ID));
        Assert.assertTrue(repository.existsById(USUAL_USER.getId(), USUAL_SESSION1.getId()));
    }

    @Test
    public void findOneById() throws AbstractFieldException {
        Assert.assertNull(repository.findOneById(null));
        Assert.assertNull(repository.findOneById(NON_EXISTING_SESSION_ID));
        @Nullable final Session session = repository.findOneById(USUAL_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USUAL_SESSION1, session);
    }

    @Test
    public void findOneByIdByUserId() throws AbstractFieldException {
        Assert.assertNull(repository.findOneById(USUAL_USER.getId(), NON_EXISTING_SESSION_ID));
        @Nullable final Session session = repository.findOneById(USUAL_USER.getId(), USUAL_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USUAL_SESSION1, session);
    }

    @Test
    public void findOneByIndex() throws AbstractFieldException {
        final int index = repository.findAll().indexOf(USUAL_SESSION1);
        @Nullable final Session session = repository.findOneByIndex(index);
        Assert.assertNotNull(session);
        Assert.assertEquals(USUAL_SESSION1, session);
    }

    @Test
    public void findOneByIndexByUserId() throws AbstractFieldException {
        final int index = repository.findAll(USUAL_USER.getId()).indexOf(USUAL_SESSION1);
        @Nullable final Session session = repository.findOneByIndex(USUAL_USER.getId(), index);
        Assert.assertNotNull(session);
        Assert.assertEquals(USUAL_SESSION1, session);
    }

    @Test
    public void clearByUserId() throws UserIdEmptyException, SQLException {
        repository.clear(USUAL_USER.getId());
        Assert.assertEquals(0, repository.getSize(USUAL_USER.getId()));
        connection.rollback();
    }

    @Test
    public void remove() throws AbstractFieldException, AccessDeniedException {
        Assert.assertNull(repository.removeOne(null));
        @Nullable final Session createdSession = repository.add(ADMIN_SESSION1);
        @Nullable final Session removedSession = repository.removeOne(createdSession);
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(ADMIN_SESSION1, removedSession);
        Assert.assertNull(repository.findOneById(ADMIN_SESSION1.getId()));
    }

    @Test
    public void removeByUserId() throws AbstractFieldException {
        Assert.assertNull(repository.remove(ADMIN_USER.getId(), null));
        @Nullable final Session createdSession = repository.add(ADMIN_SESSION1);
        Assert.assertNull(repository.remove(null, createdSession));
        @Nullable final Session removedSession = repository.remove(ADMIN_USER.getId(), createdSession);
        Assert.assertEquals(ADMIN_SESSION1, removedSession);
        Assert.assertNull(repository.findOneById(ADMIN_USER.getId(), ADMIN_SESSION1.getId()));
    }

    @Test
    public void removeById() throws AbstractFieldException {
        Assert.assertNull(repository.removeById(null));
        Assert.assertNull(repository.removeById(NON_EXISTING_SESSION_ID));
        repository.add(ADMIN_SESSION1);
        @Nullable final Session removedSession = repository.removeById(ADMIN_SESSION1.getId());
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(ADMIN_SESSION1, removedSession);
        Assert.assertNull(repository.findOneById(ADMIN_SESSION1.getId()));
    }

    @Test
    public void removeByIdByUserId() throws AbstractFieldException {
        Assert.assertNull(repository.removeById(ADMIN_USER.getId(), null));
        Assert.assertNull(repository.removeById(ADMIN_USER.getId(), NON_EXISTING_SESSION_ID));
        Assert.assertNull(repository.removeById(ADMIN_USER.getId(), USUAL_SESSION1.getId()));
        @Nullable final Session createdSession = repository.add(ADMIN_SESSION1);
        Assert.assertNull(repository.removeById(null, createdSession.getId()));
        @Nullable final Session removedSession = repository.removeById(ADMIN_USER.getId(), createdSession.getId());
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(ADMIN_SESSION1, removedSession);
        Assert.assertNull(repository.findOneById(ADMIN_USER.getId(), ADMIN_SESSION1.getId()));
    }

    @Test
    public void removeByIndex() throws AbstractFieldException {
        Assert.assertNull(repository.removeByIndex(null));
        @Nullable final Session createdSession = repository.add(ADMIN_SESSION1);
        final int index = repository.findAll().indexOf(createdSession);
        @Nullable final Session removedSession = repository.removeByIndex(index);
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(ADMIN_SESSION1, removedSession);
        Assert.assertNull(repository.findOneById(ADMIN_SESSION1.getId()));
    }

    @Test
    public void removeByIndexByUserId() throws AbstractFieldException {
        Assert.assertNull(repository.removeByIndex(ADMIN_USER.getId(), null));
        @Nullable final Session createdSession = repository.add(ADMIN_SESSION1);
        final int index = repository.findAll(ADMIN_USER.getId()).indexOf(createdSession);
        Assert.assertNull(repository.removeByIndex(null, index));
        @Nullable final Session removedSession = repository.removeByIndex(ADMIN_USER.getId(), index);
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(ADMIN_SESSION1, removedSession);
        Assert.assertNull(repository.findOneById(ADMIN_USER.getId(), ADMIN_SESSION1.getId()));
    }

    @Test
    public void getSizeByUserId() throws UserIdEmptyException, SQLException {
        Assert.assertEquals(0, repository.getSize(ADMIN_USER.getId()));
        repository.add(ADMIN_SESSION1);
        Assert.assertEquals(1, repository.getSize(ADMIN_USER.getId()));
        connection.rollback();
    }

    @Test
    public void removeAll() throws AccessDeniedException, UserIdEmptyException {
        repository.add(ADMIN_SESSION_LIST);
        Assert.assertEquals(2, repository.getSize(ADMIN_USER.getId()));
        repository.removeAll(ADMIN_SESSION_LIST);
        Assert.assertEquals(0, repository.getSize(ADMIN_USER.getId()));
    }

}