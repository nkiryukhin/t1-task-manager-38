package ru.t1.nkiryukhin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.nkiryukhin.tm.api.service.IConnectionService;
import ru.t1.nkiryukhin.tm.api.service.IPropertyService;
import ru.t1.nkiryukhin.tm.api.service.ISessionService;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.field.AbstractFieldException;
import ru.t1.nkiryukhin.tm.exception.field.IdEmptyException;
import ru.t1.nkiryukhin.tm.exception.field.IndexIncorrectException;
import ru.t1.nkiryukhin.tm.exception.field.UserIdEmptyException;
import ru.t1.nkiryukhin.tm.exception.user.AccessDeniedException;
import ru.t1.nkiryukhin.tm.marker.UnitCategory;
import ru.t1.nkiryukhin.tm.model.Session;

import static ru.t1.nkiryukhin.tm.data.SessionTestData.*;
import static ru.t1.nkiryukhin.tm.data.UserTestData.ADMIN_USER;
import static ru.t1.nkiryukhin.tm.data.UserTestData.USUAL_USER;

@Category(UnitCategory.class)
public final class SessionServiceTest {

    @NotNull static final IPropertyService propertyService = new PropertyService();

    @NotNull static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull final ISessionService service = new SessionService(connectionService);

    @Before
    public void before() {
        service.add(USUAL_SESSION1);
        service.add(USUAL_SESSION2);
    }

    @After
    public void after() throws AbstractException {
        service.removeAll(SESSION_LIST);
    }

    @AfterClass
    public static void afterClass() {
        connectionService.closeDatabaseConnectionPool();
    }

    @Test
    public void add() throws AbstractFieldException {
        Assert.assertNull(service.add(NULL_SESSION));
        Assert.assertNotNull(service.add(ADMIN_SESSION1));
        @Nullable final Session session = service.findOneById(ADMIN_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(ADMIN_SESSION1, session);
    }

    @Test
    public void addByUserId() throws AbstractFieldException {
        Assert.assertNull(service.add(ADMIN_USER.getId(), NULL_SESSION));
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.add(null, ADMIN_SESSION1);
        });
        Assert.assertNotNull(service.add(ADMIN_USER.getId(), ADMIN_SESSION1));
        @Nullable final Session session = service.findOneById(ADMIN_USER.getId(), ADMIN_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(ADMIN_SESSION1, session);
    }

    @Test
    public void addMany() throws AbstractFieldException {
        Assert.assertNotNull(service.add(ADMIN_SESSION_LIST));
        for (final Session session : ADMIN_SESSION_LIST)
            Assert.assertEquals(session, service.findOneById(session.getId()));
    }

    @Test
    public void clearByUserId() throws UserIdEmptyException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.clear(null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.clear("");
        });
        Assert.assertEquals(2, service.getSize(USUAL_USER.getId()));
        service.clear(USUAL_USER.getId());
        Assert.assertEquals(0, service.getSize(USUAL_USER.getId()));
    }

    @Test
    public void existsById() {
        Assert.assertFalse(service.existsById(""));
        Assert.assertFalse(service.existsById(null));
        Assert.assertFalse(service.existsById(NON_EXISTING_SESSION_ID));
        Assert.assertTrue(service.existsById(USUAL_SESSION1.getId()));
    }

    @Test
    public void existsByIdByUserId() throws AbstractFieldException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById(null, NON_EXISTING_SESSION_ID);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById("", NON_EXISTING_SESSION_ID);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.existsById(USUAL_USER.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.existsById(USUAL_USER.getId(), "");
        });
        Assert.assertFalse(service.existsById(USUAL_USER.getId(), NON_EXISTING_SESSION_ID));
        Assert.assertTrue(service.existsById(USUAL_USER.getId(), USUAL_SESSION1.getId()));
    }

    @Test
    public void findAll() throws UserIdEmptyException {
        service.add(ADMIN_SESSION_LIST);
        Assert.assertEquals(ADMIN_SESSION_LIST, service.findAll(ADMIN_USER.getId()));
    }

    @Test
    public void findAllByUserId() throws UserIdEmptyException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findAll("");
        });
    }


    @Test
    public void findOneById() throws AbstractFieldException {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById("");
        });
        Assert.assertNull(service.findOneById(NON_EXISTING_SESSION_ID));
        @Nullable final Session session = service.findOneById(USUAL_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USUAL_SESSION1, session);
    }

    @Test
    public void findOneByIdByUserId() throws AbstractFieldException {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(USUAL_USER.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(USUAL_USER.getId(), "");
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById(null, USUAL_SESSION1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById("", USUAL_SESSION1.getId());
        });
        Assert.assertNull(service.findOneById(USUAL_USER.getId(), NON_EXISTING_SESSION_ID));
        @Nullable final Session session = service.findOneById(USUAL_USER.getId(), USUAL_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USUAL_SESSION1, session);
    }

    @Test
    public void findOneByIndex() throws AbstractFieldException {
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.findOneByIndex(null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.findOneByIndex(-1);
        });
        final int index = service.findAll().indexOf(USUAL_SESSION1);
        @Nullable final Session session = service.findOneByIndex(index);
        Assert.assertNotNull(session);
        Assert.assertEquals(USUAL_SESSION1, session);
    }

    @Test
    public void findOneByIndexByUserId() throws AbstractFieldException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findOneByIndex(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findOneByIndex("", null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.findOneByIndex(USUAL_USER.getId(), null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.findOneByIndex(USUAL_USER.getId(), -1);
        });
        final int index = service.findAll(USUAL_USER.getId()).indexOf(USUAL_SESSION1);
        @Nullable final Session session = service.findOneByIndex(USUAL_USER.getId(), index);
        Assert.assertNotNull(session);
        Assert.assertEquals(USUAL_SESSION1, session);
    }

    @Test
    public void getSize() {
        int initCount = service.getSize();
        service.add(ADMIN_SESSION1);
        Assert.assertEquals(initCount + 1, service.getSize());
    }

    @Test
    public void getSizeByUserId() throws UserIdEmptyException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.getSize(null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.getSize("");
        });
        Assert.assertEquals(0, service.getSize(ADMIN_USER.getId()));
        service.add(ADMIN_SESSION1);
        Assert.assertEquals(1, service.getSize(ADMIN_USER.getId()));
    }

    @Test
    public void remove() throws AbstractException {
        Assert.assertNull(service.removeOne(null));
        @Nullable final Session createdSession = service.add(ADMIN_SESSION1);
        @Nullable final Session removedSession = service.removeOne(createdSession);
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(ADMIN_SESSION1, removedSession);
        Assert.assertNull(service.findOneById(ADMIN_SESSION1.getId()));
    }

    @Test
    public void removeByUserId() throws AbstractFieldException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.remove(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.remove("", null);
        });
        Assert.assertNull(service.remove(ADMIN_USER.getId(), null));
        @Nullable final Session createdSession = service.add(ADMIN_SESSION1);
        @Nullable final Session removedSession = service.remove(ADMIN_USER.getId(), createdSession);
        Assert.assertEquals(ADMIN_SESSION1, removedSession);
        Assert.assertNull(service.findOneById(ADMIN_USER.getId(), ADMIN_SESSION1.getId()));
    }

    @Test
    public void removeById() throws AbstractFieldException {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById("");
        });
        Assert.assertNull(service.removeById(NON_EXISTING_SESSION_ID));
        service.add(ADMIN_SESSION1);
        @Nullable final Session removedSession = service.removeById(ADMIN_SESSION1.getId());
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(ADMIN_SESSION1, removedSession);
        Assert.assertNull(service.findOneById(ADMIN_SESSION1.getId()));
    }

    @Test
    public void removeByIdByUserId() throws AbstractFieldException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeById(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeById("", null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(USUAL_USER.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(USUAL_USER.getId(), "");
        });
        Assert.assertNull(service.removeById(ADMIN_USER.getId(), NON_EXISTING_SESSION_ID));
        Assert.assertNull(service.removeById(ADMIN_USER.getId(), USUAL_SESSION1.getId()));
        @Nullable final Session createdSession = service.add(ADMIN_SESSION1);
        @Nullable final Session removedSession = service.removeById(ADMIN_USER.getId(), createdSession.getId());
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(ADMIN_SESSION1, removedSession);
        Assert.assertNull(service.findOneById(ADMIN_USER.getId(), ADMIN_SESSION1.getId()));
    }

    @Test
    public void removeByIndex() throws AbstractFieldException {
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex(null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex(-1);
        });
        @Nullable final Session createdSession = service.add(ADMIN_SESSION1);
        final int index = service.findAll().indexOf(createdSession);
        @Nullable final Session removedSession = service.removeByIndex(index);
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(ADMIN_SESSION1, removedSession);
        Assert.assertNull(service.findOneById(ADMIN_SESSION1.getId()));
    }

    @Test
    public void removeByIndexByUserId() throws AbstractFieldException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeByIndex(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeByIndex("", null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex(USUAL_USER.getId(), null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex(USUAL_USER.getId(), -1);
        });
        @Nullable final Session createdSession = service.add(ADMIN_SESSION1);
        final int index = service.findAll(ADMIN_USER.getId()).indexOf(createdSession);
        @Nullable final Session removedSession = service.removeByIndex(ADMIN_USER.getId(), index);
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(ADMIN_SESSION1, removedSession);
        Assert.assertNull(service.findOneById(ADMIN_USER.getId(), ADMIN_SESSION1.getId()));
    }

    @Test
    public void removeAll() throws AccessDeniedException, UserIdEmptyException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            String userId = null;
            service.removeAll(userId);
        });
        int count = service.getSize();
        service.add(ADMIN_SESSION_LIST);
        service.removeAll(SESSION_LIST);
        Assert.assertEquals(count - 2, service.getSize());
    }

}

