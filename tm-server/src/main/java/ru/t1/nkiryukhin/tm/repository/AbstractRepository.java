package ru.t1.nkiryukhin.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.api.repository.IRepository;
import ru.t1.nkiryukhin.tm.comparator.CreatedComparator;
import ru.t1.nkiryukhin.tm.comparator.StatusComparator;
import ru.t1.nkiryukhin.tm.model.AbstractModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final Connection connection;

    protected AbstractRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @NotNull
    protected abstract String getTableName();

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        else if (comparator == StatusComparator.INSTANCE) return "status";
        else return "name";
    }

    @NotNull
    protected abstract M fetch(@NotNull final ResultSet row);

    @NotNull
    @Override
    public abstract M add(@NotNull final M model);

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        List<M> result = new ArrayList<>();
        for (M model : models) {
            result.add(add(model));
        }
        return result;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) {
        clear();
        return add(models);
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final String sql = String.format("DELETE FROM %s", getTableName());
        try (@NotNull final Statement statement = connection.createStatement()) {
            statement.execute(sql);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s", getTableName());
        try (@NotNull final Statement statement = connection.createStatement();
             @NotNull final ResultSet resultSet = statement.executeQuery(sql)) {
            while (resultSet.next()) result.add(fetch(resultSet));
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@NotNull final Comparator<M> comparator) {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s ORDER BY %s", getTableName(), getSortType(comparator));
        try (@NotNull final Statement statement = connection.createStatement();
             @NotNull final ResultSet resultSet = statement.executeQuery(sql)) {
            while (resultSet.next()) result.add(fetch(resultSet));
        }
        return result;
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@Nullable final String id) {
        if (id == null) return null;
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE id = ? LIMIT 1", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneByIndex(@NotNull final Integer index) {
        @NotNull final String sql = String.format("SELECT * FROM %s LIMIT ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, index + 1);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            for (int i = 1; i <= index; i++) {
                rowSet.next();
            }
            return fetch(rowSet);
        }
    }

    @Override
    @SneakyThrows
    public int getSize() {
        @NotNull final String query = String.format("SELECT COUNT(1) FROM %s", getTableName());
        try (@NotNull final Statement statement = connection.createStatement();
             @NotNull final ResultSet resultSet = statement.executeQuery(query)) {
            resultSet.next();
            return resultSet.getInt("count");
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeOne(@Nullable final M model) {
        if (model == null) return null;
        @NotNull final String sql = String.format("DELETE FROM %s WHERE id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.executeUpdate();
        }
        return model;
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String id) {
        final M model = findOneById(id);
        if (model == null) return null;
        removeOne(model);
        return model;
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable Integer index) {
        if (index == null) return null;
        @NotNull final M model = findOneByIndex(index);
        removeOne(model);
        return model;
    }

    @Override
    public void removeAll(@NotNull final Collection<M> collection) {
        for (M model : collection) {
            removeOne(model);
        }
    }

}
