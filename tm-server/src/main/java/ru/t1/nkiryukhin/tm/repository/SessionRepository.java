package ru.t1.nkiryukhin.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.api.repository.ISessionRepository;
import ru.t1.nkiryukhin.tm.enumerated.Role;
import ru.t1.nkiryukhin.tm.model.Session;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    @NotNull
    private static final String table = "tm_session";

    public SessionRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Override
    protected String getTableName() {
        return table;
    }

    @NotNull
    @Override
    @SneakyThrows
    protected Session fetch(@NotNull final ResultSet row) {
        @NotNull final Session session = new Session();
        session.setId(row.getString("id"));
        session.setDate(row.getTimestamp("created"));
        session.setUserId(row.getString("user_id"));
        session.setRole(Role.valueOf(row.getString("role")));
        return session;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session add(@NotNull final Session session) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (id, created, role, user_id) VALUES (?, ?, ?::role, ?)",
                getTableName()
        );
        try(@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, session.getId());
            statement.setTimestamp(2, new Timestamp(session.getDate().getTime()));
            statement.setString(3, session.getRole().name());
            statement.setString(4, session.getUserId());
            statement.executeUpdate();
        }
        return session;
    }

    @Nullable
    @Override
    public Session add(@Nullable final String userId, @Nullable final Session session) {
        session.setUserId(userId);
        return add(session);
    }

}
