package ru.t1.nkiryukhin.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.api.repository.IUserOwnedRepository;
import ru.t1.nkiryukhin.tm.api.service.IConnectionService;
import ru.t1.nkiryukhin.tm.api.service.IUserOwnedService;
import ru.t1.nkiryukhin.tm.enumerated.Sort;
import ru.t1.nkiryukhin.tm.exception.field.AbstractFieldException;
import ru.t1.nkiryukhin.tm.exception.field.IdEmptyException;
import ru.t1.nkiryukhin.tm.exception.field.IndexIncorrectException;
import ru.t1.nkiryukhin.tm.exception.field.UserIdEmptyException;
import ru.t1.nkiryukhin.tm.exception.user.AccessDeniedException;
import ru.t1.nkiryukhin.tm.model.AbstractUserOwnedModel;

import java.sql.Connection;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected abstract IUserOwnedRepository<M> getRepository(@NotNull final Connection connection);

    @Nullable
    @Override
    @SneakyThrows
    public M add(@Nullable final String userId, @Nullable final M model) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return null;
        @NotNull final Connection connection = getConnection();
        @Nullable final M result;
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            result = repository.add(userId, model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<M> add(@Nullable final String userId, @NotNull final Collection<M> models) {
        if (userId == null || models == null || models.isEmpty()) return Collections.emptyList();
        for (M model : models) {
            add(userId, model);
        }
        return models;
    }

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            repository.clear(userId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            return repository.existsById(userId, id);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            return repository.findAll(userId);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(
            @Nullable final String userId,
            @Nullable final Comparator<M> comparator
    ) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll(userId);
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            return repository.findAll(userId, comparator);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId, @Nullable final Sort sort) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            return repository.findAll(userId, sort.getComparator());
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@Nullable final String userId, @Nullable final String id) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            return repository.findOneById(userId, id);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            return repository.findOneByIndex(userId, index);
        }
    }

    @Override
    @SneakyThrows
    public int getSize(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            return repository.getSize(userId);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M remove(@Nullable final String userId, @Nullable final M model) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return null;
        @NotNull final Connection connection = getConnection();
        @Nullable final M result;
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            result = repository.remove(userId, model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeById(@Nullable final String userId, @Nullable final String id) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Connection connection = getConnection();
        @Nullable final M result;
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            result = repository.removeById(userId, id);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeByIndex(@Nullable final String userId, @Nullable final Integer index) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        @NotNull final Connection connection = getConnection();
        @Nullable final M result;
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            result = repository.removeByIndex(userId, index);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @Override
    public void removeAll(@Nullable final String userId) throws UserIdEmptyException, AccessDeniedException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull List<M> list = findAll(userId);
        removeAll(list);
    }

}
