package ru.t1.nkiryukhin.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IDatabaseProperty {

    @NotNull
    String getDatabaseName();

    @NotNull
    String getDatabaseUser();

    @NotNull
    String getDatabasePassword();

    @NotNull
    String getDatabaseUrl();

    @NotNull
    String getDatabaseServerUrl();

    @NotNull
    int getMinIdleConnections();

    @NotNull
    int getMaxIdleConnections();

    @NotNull
    int getMaxPreparedStatements();

}