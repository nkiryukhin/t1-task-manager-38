package ru.t1.nkiryukhin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.exception.field.AbstractFieldException;
import ru.t1.nkiryukhin.tm.exception.field.UserIdEmptyException;
import ru.t1.nkiryukhin.tm.exception.user.AccessDeniedException;
import ru.t1.nkiryukhin.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    @Nullable
    M add(@NotNull String userId, @NotNull M model) throws UserIdEmptyException;

    void clear(@NotNull String userId) throws UserIdEmptyException;

    boolean existsById(@Nullable String userId, @Nullable String id) throws AbstractFieldException;

    @NotNull
    List<M> findAll(@NotNull String userId) throws UserIdEmptyException;

    @NotNull
    List<M> findAll(@NotNull String userId, @Nullable Comparator<M> comparator) throws UserIdEmptyException;

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id) throws AbstractFieldException;

    @Nullable
    M findOneByIndex(@NotNull String userId, @NotNull Integer index) throws AbstractFieldException;

    int getSize(@NotNull String userId) throws UserIdEmptyException;

    @Nullable
    M removeById(@Nullable String userId, @Nullable String id) throws AbstractFieldException;

    @Nullable
    M removeByIndex(@Nullable String userId, @Nullable Integer index) throws AbstractFieldException;

    @Nullable
    M remove(@Nullable String userId, @Nullable M model) throws UserIdEmptyException;

    void removeAll(@Nullable String userId) throws UserIdEmptyException, AccessDeniedException;

}