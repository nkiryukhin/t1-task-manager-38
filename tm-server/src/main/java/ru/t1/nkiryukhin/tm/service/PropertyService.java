package ru.t1.nkiryukhin.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Cleanup;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.api.service.IPropertyService;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import static java.lang.ClassLoader.getSystemResourceAsStream;

public final class PropertyService implements IPropertyService {

    @NotNull
    public static final String APPLICATION_FILE_NAME_DEFAULT = "application.properties";

    @NotNull
    public static final String APPLICATION_FILE_NAME_KEY = "config";

    @NotNull
    public static final String DATABASE_NAME_KEY = "database.name";

    @NotNull
    public static final String DATABASE_NAME_DEFAULT = "tm";

    @NotNull
    public static final String DATABASE_USER_KEY = "database.username";

    @NotNull
    public static final String DATABASE_USER_DEFAULT = "admin";

    @NotNull
    public static final String DATABASE_PASSWORD_KEY = "database.password";

    @NotNull
    public static final String DATABASE_PASSWORD_DEFAULT = "123";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "12345";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "345345345";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String EMPTY_VALUE = "---";

    @NotNull
    private static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    private static final String SERVER_PORT_DEFAULT = "6060";

    @NotNull
    private static final String SERVER_HOST_DEFAULT = "0.0.0.0";

    @NotNull
    private static final String SESSION_KEY = "session.key";

    @NotNull
    private static final String SESSION_TIMEOUT = "session.timeout";

    @NotNull
    private static final String SESSION_TIMEOUT_DEFAULT = "60000";

    @NotNull
    private static final String DATABASE_SERVER_URL_KEY = "database.server.url";

    @NotNull
    private static final String DATABASE_SERVER_URL_DEFAULT = "jdbc:postgresql://localhost:5432/";

    @NotNull
    private static final String MIN_IDLE_CONNECTIONS_KEY = "database.min_connections_in_pool";

    @NotNull
    private static final String MIN_IDLE_CONNECTIONS_DEFAULT = "5";

    @NotNull
    private static final String MAX_IDLE_CONNECTIONS_KEY = "database.max_connections_in_pool";

    @NotNull
    private static final String MAX_IDLE_CONNECTIONS_DEFAULT = "10";

    @NotNull
    private static final String MAX_OPEN_PREPARED_STATEMENTS_KEY = "database.max_open_prepared_statements";

    @NotNull
    private static final String MAX_OPEN_PREPARED_STATEMENTS_DEFAULT = "100";

    @NotNull
    private static final String DBA_PASSPHRASE_KEY = "dba.passphrase";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        final boolean existsConfig = isExistsExternalConfig();
        if (existsConfig) loadExternalConfig(properties);
        else loadInternalConfig(properties);
    }

    @NotNull
    @Override
    public String getServerHost() {
        return getStringValue(SERVER_HOST_KEY, SERVER_HOST_DEFAULT);
    }

    @NotNull
    @Override
    public String getServerPort() {
        return getStringValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
    }

    @NotNull
    @Override
    public String getSessionKey() {
        return getStringValue(SESSION_KEY);
    }

    @NotNull
    @Override
    public Integer getSessionTimeout() {
        @NotNull final String value = getStringValue(SESSION_TIMEOUT, SESSION_TIMEOUT_DEFAULT);
        return Integer.parseInt(value);
    }

    @SneakyThrows
    private void loadInternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = APPLICATION_FILE_NAME_DEFAULT;
        @Cleanup @Nullable final InputStream inputStream = getSystemResourceAsStream(name);
        if (inputStream == null) return;
        properties.load(inputStream);
    }

    @SneakyThrows
    private void loadExternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        @Cleanup @NotNull final InputStream inputStream = new FileInputStream(file);
        properties.load(inputStream);
    }

    private boolean isExistsExternalConfig(){
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        return file.exists();
    }

    @NotNull
    @Override
    public String getApplicationConfig() {
        return getStringValue(APPLICATION_FILE_NAME_KEY, APPLICATION_FILE_NAME_DEFAULT);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return getIntegerValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return "1.37.0";
        //return read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return "nkiryukhin@t1-consulting.ru";
        //return read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return "Nikita Kiryuhin";
        //return read(AUTHOR_NAME_KEY);
    }

    @NotNull
    private String read(@Nullable final String key) {
        if (key == null || key.isEmpty()) return EMPTY_VALUE;
        if (!Manifests.exists(key)) return EMPTY_VALUE;
        return Manifests.read(key);
    }

    @NotNull
    private Integer getIntegerValue(@NotNull final String key, @NotNull final String defaultValue) {
        return Integer.parseInt(getStringValue(key, defaultValue));
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDatabaseName() {
        return getStringValue(DATABASE_NAME_KEY, DATABASE_NAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseUser() {
        return getStringValue(DATABASE_USER_KEY, DATABASE_USER_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabasePassword() {
        return getStringValue(DATABASE_PASSWORD_KEY, DATABASE_PASSWORD_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseServerUrl() {
        return getStringValue(DATABASE_SERVER_URL_KEY, DATABASE_SERVER_URL_DEFAULT);
    }

    @NotNull
    @Override
    public int getMinIdleConnections() {
        return getIntegerValue(MIN_IDLE_CONNECTIONS_KEY, MIN_IDLE_CONNECTIONS_DEFAULT);
    }

    @NotNull
    @Override
    public int getMaxIdleConnections() {
        return getIntegerValue(MAX_IDLE_CONNECTIONS_KEY, MAX_IDLE_CONNECTIONS_DEFAULT);
    }

    @NotNull
    @Override
    public int getMaxPreparedStatements() {
        return getIntegerValue(MAX_OPEN_PREPARED_STATEMENTS_KEY, MAX_OPEN_PREPARED_STATEMENTS_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseUrl() {
        String databaseUrl = getDatabaseServerUrl() + getDatabaseName();
        return databaseUrl;
    }

    @NotNull
    @Override
    public String getDbaPassphrase() {
        return getStringValue(DBA_PASSPHRASE_KEY);
    }

}
