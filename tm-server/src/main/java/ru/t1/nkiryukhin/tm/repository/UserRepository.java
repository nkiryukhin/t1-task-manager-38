package ru.t1.nkiryukhin.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.api.repository.IUserRepository;
import ru.t1.nkiryukhin.tm.enumerated.Role;
import ru.t1.nkiryukhin.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    private static final String table = "tm_user";

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return table;
    }

    @NotNull
    @Override
    @SneakyThrows
    protected User fetch(@NotNull final ResultSet row) {
        @NotNull final User user = new User();
        user.setId(row.getString("id"));
        user.setLogin(row.getString("login"));
        user.setPasswordHash(row.getString("password"));
        user.setEmail(row.getString("email"));
        user.setLocked(row.getBoolean("locked"));
        user.setFirstName(row.getString("first_name"));
        user.setLastName(row.getString("last_name"));
        user.setMiddleName(row.getString("middle_name"));
        user.setRole(Role.valueOf(row.getString("role")));
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User add(@NotNull final User user) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (id, login, password, email, locked, first_name, last_name, middle_name, role) " +
                        "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?::role)",
                getTableName()
        );
        try(@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getId());
            statement.setString(2, user.getLogin());
            statement.setString(3, user.getPasswordHash());
            statement.setString(4, user.getEmail());
            statement.setBoolean(5, user.getLocked());
            statement.setString(6, user.getFirstName());
            statement.setString(7, user.getLastName());
            statement.setString(8, user.getMiddleName());
            statement.setString(9, user.getRole().name());
            statement.executeUpdate();
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        if (login == null) return null;
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE login = ? LIMIT 1", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@Nullable final String email) {
        if (email == null) return null;
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE email = ? LIMIT 1", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, email);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean isLoginExist(@Nullable final String login) {
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@Nullable final String email) {
        return findByEmail(email) != null;
    }

    @Override
    @SneakyThrows
    public void update(@NotNull final User user) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET login = ?, password = ?, email = ?, locked = ?, first_name = ?, last_name = ?, " +
                        "middle_name = ?, role = ?::role" +
                        " WHERE id = ?",
                getTableName()
        );
        try(@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPasswordHash());
            statement.setString(3, user.getEmail());
            statement.setBoolean(4, user.getLocked());
            statement.setString(5, user.getFirstName());
            statement.setString(6, user.getLastName());
            statement.setString(7, user.getMiddleName());
            statement.setString(8, user.getRole().toString());
            statement.setString(9, user.getId());
            statement.executeUpdate();
        }
    }

}