package ru.t1.nkiryukhin.tm.service;

import lombok.SneakyThrows;
import org.apache.commons.dbcp2.BasicDataSource;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.api.service.IConnectionService;
import ru.t1.nkiryukhin.tm.api.service.IDatabaseProperty;

import java.sql.Connection;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IDatabaseProperty databaseProperty;

    @Nullable
    private BasicDataSource dataSource;

    public ConnectionService(@NotNull final IDatabaseProperty databaseProperty) {
        this.databaseProperty = databaseProperty;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Connection getConnection() {
        if (dataSource == null || dataSource.isClosed()) openDatabaseConnectionPool();
        @NotNull final Connection connection = dataSource.getConnection();
        connection.setAutoCommit(false);
        return connection;
    }

    @SneakyThrows
    public void openDatabaseConnectionPool() {
        if (dataSource != null) dataSource.close();
        @NotNull final String url = databaseProperty.getDatabaseUrl();
        @NotNull final String username = databaseProperty.getDatabaseUser();
        @NotNull final String password = databaseProperty.getDatabasePassword();
        @NotNull final int minIdleConnections = databaseProperty.getMinIdleConnections();
        @NotNull final int maxIdleConnections = databaseProperty.getMaxIdleConnections();
        @NotNull final int maxOpenPreparedStatements = databaseProperty.getMaxPreparedStatements();
        dataSource = new BasicDataSource();
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        dataSource.setMinIdle(minIdleConnections);
        dataSource.setMaxIdle(maxIdleConnections);
        dataSource.setAutoCommitOnReturn(false);
        dataSource.setMaxOpenPreparedStatements(maxOpenPreparedStatements);
    }

    @SneakyThrows
    public void closeDatabaseConnectionPool() {
        if (dataSource == null) return;
        dataSource.close();
    }

}
