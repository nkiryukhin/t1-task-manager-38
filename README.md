# TASK MANAGER

## DEVELOPER INFO

**NAME**: Nikita Kiryukhin

**E-MAIL**: nkiryukhin@t1-consulting.ru

**E-MAIL**: n.kiryukhin@inbox.ru

## SOFTWARE

**OS**: Microsoft Windows 10.0.17763

**JDK**: OPENJDK 1.8.0_322

## HARDWARE

**CPU**: Intel Core i5-10210U

**RAM**: 16GB

**SSD**: 256GB

## BUILD PROGRAM

```powershell
mvn clean install
```

## RUN PROGRAM

```powershell
java -jar task-manager.jar
```
